from faker import Faker
import pymysql
import random
from datetime import datetime

def SetLanguage(language):
    global fake
    if(language == "ru"):
        fake = Faker('ru_RU')
        print("Current language: ru")
    elif(language == "en"):
        fake = Faker('en_US')
        print("Current language: en")

def AutoConnect():
    global conn
    global coursor
    conn = pymysql.connect(host = "127.0.0.1", port = 3306, user = "root", passwd = "root", charset = 'utf8',db = "optcompany")
    coursor = conn.cursor()
    print("Connect: " + "127.0.0.1:3306" + " BD: " + "optcompany")

def Connect(_host, _port, _user, _password, _charset, _bdName):
    global conn
    global coursor
    conn = pymysql.connect(host = _host, port = int(_port), user = _user, passwd = _password, charset = _charset, db = _bdName)
    coursor = conn.cursor()
    print("Connect: " + _host + ":" + _port + " BD: " + _bdName)

def Disconnect():
    conn.close()
    coursor.close()
    print("Disconect...")
    

def GetClients(show):
    sql = "SELECT `ID`, `NumberClient` FROM `client`"
    return SelectOn(sql, show, "Clients")

def GetCategoryProducts(show):
    sql = "SELECT `ID`, `CategoryName` FROM `categoryproduct`"
    return SelectOn(sql, show, "Category products")

def GetUnits(show):
    sql = "SELECT `ID`, `UnitName` FROM `unit`"
    return SelectOn(sql, show, "Unit")

def GetInvoice(show):
    sql = "SELECT `ID`, `NumberInvoice` FROM `invoice`"
    return SelectOn(sql, show, "Invoice")
    
def GetProduct(show):
    sql = "SELECT `ID`, `ProductName`, `BasePrice` FROM `product`"
    return SelectOn(sql, show, "Product")

def GetPhoneCategory(show):
    sql = "SELECT `ID`, `CategoryPhoneName` FROM `категория_телефона`"
    return SelectOn(sql, show, "Категория_телефона")

def SelectOn(sql, show, name):
    coursor.execute(sql)
    data = coursor.fetchall()
    if(show == True):
        print("--" + name + "--")
        print(data)
        print("Length: " + "0 - " + str(len(data) - 1))
    return data
    


def SetCategoryProduct(count, autoID = False, noParentId = True):
    print("--Set CategoryProduct--")
    ID = "0"
    parentID = "null"
    
    if(autoID == False):
        print("Enter start ID of auto add")
        ID = input()
        ID = int(ID)
        
    for i in range (count):
        if(autoID == True):
            ID = fake.plate_number()
            ID = int(ID)
            
        if(noParentId == False):    
            print("Enter start parentID of auto add")
            parentID = input()
            
        categoryName = fake.uri_page()
        
        if(parentID == "null"):
            sql = "INSERT INTO `categoryproduct`(`ID`, `CategoryName`) VALUES(%s,%s)"
            coursor.execute(sql,(ID, categoryName,))
        else:
            parentID = int(parentID)
            sql = "INSERT INTO `categoryproduct`(`ID`, `CategoryName`, `ParentID`) VALUES(%s,%s,%s)"
            coursor.execute(sql,(ID, categoryName, int(parentID)))
        ID = ID + 1
        conn.commit() 
    print("Added data on categoryProduct")

def SetClients(count, autoID = False):
    print("--Set Clients--")
    ID = 0
    sql = "INSERT INTO `client`(`ID`, `NumberClient`, `Address`, `CategoryClient`) VALUES(%s,%s,%s,%s)"
    
    if(autoID == False):
        print("Enter start ID of auto add")
        ID = input()
        ID = int(ID)
        
    for i in range (count):
        if(autoID == True):
            ID = fake.plate_number()
            ID = int(ID)
            
        numClient = fake.credit_card_number()
        anddress = fake.address()
        categoryBit = random.random()
        
        coursor.execute(sql,(ID, numClient, anddress, int(categoryBit)))
        conn.commit()
        ID = ID + 1
    print("Added data on clients")

def SetEntity(count, autoFill = False, showClientsID = True):
    print("--Set Entity--")
    indexClientID = 0
    clients = GetClients(showClientsID)
    sql = "INSERT INTO `entity`(`ID`, `EntityName`, `EntityAddress`, `Property`) VALUES(%s,%s,%s,%s)"

    if(autoFill == True):
        print("Enter start clientID index")
        indexClientID = input()
        indexClientID = int(indexClientID)
        
    for i in range (count):
        if(autoFill == False):
            print("Select client id and write index")
            indexClientID = input()
            indexClientID = int(indexClientID)

        name = fake.company()
        address = fake.address()
        prop = fake.catch_phrase() + " " + fake.bs()

        coursor.execute(sql,(int(clients[indexClientID][0]), name, address, prop)) #warning!!!
        conn.commit()
        indexClientID = indexClientID + 1
    print("Added data on entity")
    
def SetInvoice(count, autoID = False, autoFill = False, showClientsID = True):
    print("--Set Invoice--")
    ID = 0
    indexClientID = 0
    clients = GetClients(showClientsID)
    sql = "INSERT INTO `invoice`(`ID`, `NumberInvoice`, `DateCreation`, `TimeCreation`, `ID_Client`) VALUES(%s,%s,%s,%s,%s)"

    if(autoID == False):
        print("Enter start ID of auto add")
        ID = input()
        ID = int(ID)
    
    if(autoFill == True):
        print("Enter start clientID index")
        indexClientID = input()
        indexClientID = int(indexClientID)
    
    for i in range(count):
        if(autoID == True):
            ID = fake.plate_number()
            ID = int(ID)
            
        if(autoFill == False):
            print("Select client id and write index")
            indexClientID = input()
            indexClientID = int(indexClientID)
            
        number = fake.aba()
        date = fake.date()
        time = fake.time()
        coursor.execute(sql,(int(ID),
                             int(number),
                             datetime.strptime(date, '%Y-%m-%d'),
                             datetime.strptime(time, '%H:%M:%S'),
                             int(clients[indexClientID][0]))) #warning!!!
        conn.commit()
        ID = ID + 1
        indexClientID = indexClientID + 1
    print("Added data on invoice")
    
def SetPersons(count, autoFill = False, showClients = True):
    print("--Set Person--")
    indexClientID = 0
    clients = GetClients(showClients)
    sql = "INSERT INTO `person`(`ID`, `FullName`, `PassportData`) VALUES(%s,%s,%s)"

    if(autoFill == True):
        print("Enter start clientID index")
        indexClientID = input()
        indexClientID = int(indexClientID)

    for i in range(count):
        if(autoFill == False):
            print("Select client id and write index")
            indexClientID = input()
            indexClientID = int(indexClientID)

        name = fake.name()
        passport = fake.bic() + " " + fake.administrative_unit()
        
        coursor.execute(sql,(int(clients[indexClientID][0]),name, passport)) #warning!!!
        conn.commit()
        indexClientID = indexClientID + 1
    print("Added data on persons")

def SetUnits(count, autoFill = False):
    print("--Set Unit--")
    ID = 0
    sql = "INSERT INTO `unit`(`ID`, `UnitName`) VALUES(%s,%s)"
    
    if(autoFill == True):
        print("Enter start clientID index")
        ID = input()
        ID = int(ID)
    for i in range(count):
        if(autoFill == False):
            print("Write ID value")
            ID = input()
            ID = int(ID)
            
        name = fake.color_name()
        
        coursor.execute(sql,(ID, name))
        conn.commit()
        ID = ID + 1
    print("Added data on unit")

def SetProducts(count, autoFill = False, show = True):
    print("--Set Product--")
    ID = 0
    sql = "INSERT INTO `product`(`ID`, `ProductName`, `BasePrice`, `Amount`, `ID_CategoryProduct`, `ID_Unit`) VALUES(%s,%s,%s,%s,%s,%s)"
    
    units = GetUnits(show)
    print("Write first index value Unit")
    UnitIdIndex = input()
    UnitIdIndex = int(UnitIdIndex)
    category = GetCategoryProducts(show)
    print("Write first index value Category Product")
    CategoryProductIndex = input()
    CategoryProductIndex = int(CategoryProductIndex)
    
    if(autoFill == True):
        print("Enter start ID index")
        ID = input()
        ID = int(ID)
        
    for i in range (count):
        if(autoFill == False):
            print("Write ID value")
            ID = input()
            ID = int(ID)
            print("Write next index value Unit")
            UnitIdIndex = input()
            UnitIdIndex = int(UnitIdIndex)
            print("Write next index value Category Product")
            CategoryProductIndex = input()
            CategoryProductIndex = int(CategoryProductIndex)

        name = fake.currency_name()
        price = fake.credit_card_security_code()
        amount = fake.plate_number()
        
        coursor.execute(sql,(ID, name, price, amount,int(category[CategoryProductIndex][0]),int(units[UnitIdIndex][0]))) #warning!!!
        conn.commit()
        
        UnitIdIndex = UnitIdIndex + 1
        CategoryProductIndex = CategoryProductIndex + 1
        ID = ID + 1
    print("Added data on product")

def SetInvoicePosition(count, autoFill = False, show = True):
    print("--Set InvoicePosition--")
    ID = 0
    sql = "INSERT INTO `invoiceposition`(`ID`, `NumberPosition`, `SellingPrice`, `Amount`, `ID_Invoice`, `ID_Product`) VALUES(%s,%s,%s,%s,%s,%s)"

    invoice = GetInvoice(show)
    print("Write first index value Invoice")
    invoiceIndex = input()
    invoiceIndex = int(invoiceIndex)
    
    procuts = GetProduct(show)
    print("Write first index value Product")
    productIndex = input()
    productIndex = int(productIndex)

    if(autoFill == True):
        print("Write start ID")
        ID = input()
        ID = int(ID)

    for i in range (count):
        if(autoFill == False):
            print("Write ID value")
            ID = input()
            ID = int(ID)
            print("Write next index value Invoice")
            invoiceIndex = input()
            invoiceIndex = int(invoiceIndex)
            print("Write next index value Product")
            productIndex = input()
            productIndex = int(productIndex)

        position = fake.plate_number()
        price = fake.plate_number()
        amount = fake.plate_number()
        
        coursor.execute(sql,(ID, position, price, amount,int(invoice[invoiceIndex][0]),int(procuts[productIndex][0]))) #warning!!!
        conn.commit()
        ID = ID + 1
        invoiceIndex = invoiceIndex + 1
        productIndex = productIndex + 1
    print("Added data on InvoicePosition")

def SetPhoneCategory(count, autoFill = False):
    print("--Категория_телефона--")
    ID = 0
    sql = "INSERT INTO `категория_телефона`(`ID`, `CategoryPhoneName`, `Description`) VALUES(%s,%s,%s)"

    if(autoFill == True):
        print("Write start ID")
        ID = input()
        ID = int(ID)

    for i in range(count):
        if(autoFill == False):
            print("Write ID value")
            ID = input()
            ID = int(ID)

        name = fake.country_calling_code()
        discription = fake.phone_number()
        
        coursor.execute(sql,(ID, name, discription)) 
        conn.commit()
        ID = ID + 1
    print("Added data on Категория_телефона")
    
def SetPhone(count, autoFill = False, show = True):
    print("--телефон--")
    ID = 0
    sql = "INSERT INTO `телефон`(`ID`, `PhoneNumber`, `ID_Клиент`, `ID_Категория`) VALUES(%s,%s,%s,%s)"

    clients = GetClients(show)
    print("Write first index value client")
    clientIndex = input()
    clientIndex = int(clientIndex)
    phoneCaregory = GetPhoneCategory(show)
    print("Write first index value phone category")
    phoneCategoryIndex = input()
    phoneCategoryIndex = int(phoneCategoryIndex)

    if(autoFill == True):
        print("Write start ID")
        ID = input()
        ID = int(ID)

    for i in range (count):
        if(autoFill == False):
            print("Write ID value")
            ID = input()
            ID = int(ID)
            print("Write next index value client")
            clientIndex = input()
            clientIndex = int(clientIndex)
            phoneCaregory = GetPhoneCategory(show)
            print("Write next index value phone category")
            phoneCategoryIndex = input()
            phoneCategoryIndex = int(phoneCategoryIndex)

        number = fake.phone_number()

        coursor.execute(sql,(ID, number, int(clients[clientIndex][0]),int(phoneCaregory[phoneCategoryIndex][0]))) 
        conn.commit()
        ID = ID + 1
        clientIndex = clientIndex + 1
        phoneCategoryIndex = phoneCategoryIndex + 1
    print("Added data on телефон")
    
SetLanguage("ru")
AutoConnect()

#SetCategoryProduct(10,True)
#SetClients(10,True)
#SetEntity(2, True, True)
#SetInvoice(2)
#SetPersons(4)
#SetUnits(10,True)
#SetProducts(2,True)
#SetInvoicePosition(5, True)
#SetPhoneCategory(10, True)
#SetPhone(5,True)
